// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyABJDu6IazilrtHUbMrf1_iAYLwgA7GR0Y",
    authDomain: "angularchat-c760b.firebaseapp.com",
    databaseURL: "https://angularchat-c760b-default-rtdb.firebaseio.com",
    projectId: "angularchat-c760b",
    storageBucket: "angularchat-c760b.appspot.com",
    messagingSenderId: "375098351425",
    appId: "1:375098351425:web:d2d9322498524a39b18bc0",
    measurementId: "G-DG7D2M9MYB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
